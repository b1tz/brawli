import sys
import random
import discord
from discord.ext import commands
from discord_slash.model import SlashCommandPermissionType
from discord_slash.utils.manage_commands import create_option, create_permission
from discord_slash.utils.manage_components import create_button, create_actionrow, wait_for_component
from discord_slash.model import ButtonStyle
from discord_slash.context import ComponentContext
from discord import Intents
from discord_slash import SlashCommand, SlashContext
from enum import Enum
import asyncio

client = commands.Bot(command_prefix="/", intents=Intents.default())
slash = SlashCommand(client, sync_commands=True)
token = ""
# 2 = nth 1, 3 = nth2
guild_ids = [, 870021492979675186, 870221519823994880]
bot_admin_guilds = [636150713021497344]


@client.event
async def on_ready():
    print(f"logged in as {client.user}!")


@slash.slash(name="ping", guild_ids=guild_ids)
async def _ping(ctx):
    print(f"processing {ctx.author}'s command: Ping")
    await ctx.send(f"Pong! {round(client.latency * 1000)} ms")


@slash.slash(name="exit", guild_ids=bot_admin_guilds)
@slash.permission(guild_id=bot_admin_guilds[0],
                  permissions=[
                      create_permission(810939629557121064,
                                        SlashCommandPermissionType.ROLE, True)
])
async def _exit(ctx):
    await ctx.send("Exit called. Goodbye.")
    sys.exit()


@slash.slash(name="fight",
             guild_ids=guild_ids,
             options=[
                 create_option(name="opponent",
                               description="who you want to play with",
                               option_type=6,
                               required=True)
             ],
             description="Brawli Fight!")
async def _fight(ctx, opponent: discord.User):
    if ctx.author_id == opponent.id or opponent.bot:
        ctx.send(f"Cannot play a game with {opponent.name}!")
        return

    game = Game()

    channel_msg = await ctx.send(
        f"Check your DMs! <@!{ctx.author_id}> <@!{opponent.id}>")

    # checking consent
    print("checking for consent")
    p2 = await client.fetch_user(opponent.id)
    buttons = [create_button(style=ButtonStyle.red, label="No", custom_id="no"),
               create_button(style=ButtonStyle.blue, label="Yes", custom_id="yes")]
    action_row = create_actionrow(*buttons)
    p2msg = await p2.send(content="do you want to accept the fight?", components=[action_row])

    try:
        b_ctc: ComponentContext = await wait_for_component(client, messages=p2msg, components=action_row, timeout=7)

        if b_ctc.custom_id == "yes":
            await p2msg.edit(content="Starting fight soon!", delete_after=10)
            await p2msg.delete()
            pass
        elif b_ctc.custom_id == "no":
            await p2msg.edit(content="You rejected this fight!", delete_after=10)
            await channel_msg.edit(content="Deleting this message.", delete_after=5)
            return
    except asyncio.TimeoutError:
        await p2msg.edit(content="you have timed out the fight!", delete_after=10)
        return

    await game.run(ctx, client, opponent)
    await channel_msg.edit(content=f"{game.winner} won! GG! Log:\n{game.game_log}")
    return


class FightState(Enum):
    LIGHT_ATTACK = "Light Attack",
    HEAVY_ATTACK = "Heavy Attack",
    DEFEND = "Defense",
    PLACEHOLDER = ""


class Fighter:
    hp = 100
    state = FightState.PLACEHOLDER
    dmg = 0
    name = ""

    def atk_type(self):
        if self.state is FightState.DEFEND:
            return "Defend"
        if self.state is FightState.HEAVY_ATTACK:
            return "Heavy Attack"
        if self.state is FightState.LIGHT_ATTACK:
            return "Light Attack"

    # Returns False if the thing fails to hit
    def _chance(self):
        ch = random.randint(1, 100)
        # The accuracy of a heavy attack, if ch > heavy_mid then its a miss
        heavy_mid = 60
        # The accuracy of a light attack, if ch > light_mid then its a miss
        light_mid = 90
        if self.state is FightState.HEAVY_ATTACK and ch <= heavy_mid:
            # print(f"fight state = heavy chance = {ch}")
            return True
        elif self.state is FightState.HEAVY_ATTACK and ch > heavy_mid:
            # print(f"fight state = heavy chance = {ch} missed!")
            return False

        if self.state is FightState.LIGHT_ATTACK and ch <= light_mid:
            # print(f"fight state = light chance = {ch}")
            return True
        elif self.state is FightState.LIGHT_ATTACK and ch > light_mid:
            # print(f"fight state = light chance = {ch} missed!")
            return False
        # if defend, there is 100 percent chance to hit, since its a not a dodge
        if self.state is FightState.DEFEND:
            return True

    async def atk(self, other: 'Fighter'):

        if self.state is FightState.HEAVY_ATTACK:
            hit = self._chance()
            dmg = random.randint(10, 69)
            if hit and other.state is not FightState.DEFEND:
                self.dmg = dmg
                other.hp -= self.dmg
                return self.dmg
            elif hit and other.state is FightState.DEFEND:
                self.dmg = round(float(dmg) / float(1.5))
                other.hp -= self.dmg
                return self.dmg
            else:
                self.dmg = 0
                return 0

        if self.state is FightState.LIGHT_ATTACK:
            hit = self._chance()
            dmg = random.randint(10, 40)
            if hit and other.state is not FightState.DEFEND:
                self.dmg = dmg
                other.hp -= self.dmg
                return self.dmg
            elif hit and other.state is FightState.DEFEND:
                self.dmg = round(float(dmg) / float(2))
                other.hp -= self.dmg
                return self.dmg
            else:
                self.dmg = 0
                return 0


async def fighter_state(this: Fighter, that: Fighter, button_ctx):
    if button_ctx.custom_id == "latk":
        pressed = "You did a light attack!"
        this.state = FightState.LIGHT_ATTACK
        state = this.atk_type()
        dealt = await this.atk(that)

    elif button_ctx.custom_id == "hatk":
        pressed = "You did a heavy attack!"
        this.state = FightState.HEAVY_ATTACK
        state = this.atk_type()
        dealt = await this.atk(that)

    elif button_ctx.custom_id == "def":
        pressed = "You chose to defend!"
        this.state = FightState.DEFEND
        state = this.atk_type()
        # deal no dmg here.
        dealt = 0

    return pressed, dealt, state


class Turn(Enum):
    PLAYER_1 = 0,
    PLAYER_2 = 1,
    PLACEHOLDER = 2,


def embed_new(log, self_hp, opponent_hp):
    embed = discord.embeds.Embed(title="Brawl!", color=0x7289da)
    # embed.add_field(name="Your Last Action:",value=self_last_msg ,inline=False)
    # embed.add_field(name="Opponent's Last Action: ", value=opponent_last_msg, inline=False)
    embed.add_field(name="All Previous Actions", value=log, inline=False)
    embed.add_field(name="Opponent HP", value=opponent_hp, inline=True)
    embed.add_field(name="Your HP", value=self_hp, inline=True)
    return embed


class Game:
    turn = Turn.PLAYER_1  # default player
    winner = ""
    game_log = ""

    async def run(self, ctx: SlashContext, client: commands.Bot,
                  opponent: discord.User):

        player1 = Fighter()
        player2 = Fighter()

        buttons = [
            create_button(style=ButtonStyle.red,
                          label="Light Attack",
                          custom_id="latk"),
            create_button(style=ButtonStyle.red,
                          label="Heavy Attack",
                          custom_id="hatk"),
            create_button(style=ButtonStyle.blue,
                          label="Defend",
                          custom_id="def")
        ]
        action_row = create_actionrow(*buttons)

        player1.name = ctx.author.name
        player2.name = f"{opponent.name}"

        # with these two, DMs can be called directly.
        # like
        # user_1.send("xd")
        user_1 = await client.fetch_user(ctx.author_id)  # the challenger
        user_2 = await client.fetch_user(opponent.id)  # the responder

        await user_1.send("Starting fight! Good luck!", delete_after=5)
        await user_2.send(
            f"Player {player1.name} has challenged you!\nStarting fight! Good luck!",
            delete_after=5)

        user2_message = await user_2.send(
            content=f"{player1.name} is looking over their choices, please wait!"
        )

        def quickembed():
            return discord.embeds.Embed(title="Brawl!", description="Fight!")

        def add_dmg_recv(embed: discord.embeds.Embed, got_hit_by, got_hit_for):
            return embed.add_field(name="Got Hit by",
                                   value=got_hit_by,
                                   inline=True).add_field(name="Got Hit for",
                                                          value=got_hit_for,
                                                          inline=True)

        def add_dmg_prod(embed: discord.embeds.Embed, dealt, atktype):
            return embed.add_field(name="Attacked with",
                                   value=atktype,
                                   inline=True).add_field(name="Attacked for",
                                                          value=dealt,
                                                          inline=True)

        while True:
            print(self.turn)
            # this is first turn so the initiator attacks, next time
            # next time the args are flipped in `fighter_state()`
            if self.turn is Turn.PLAYER_1:
                temp_embed = quickembed().add_field(
                    name="Choose an Attack",
                    value="Pick any of the three actions: `Heavy Attack`, `Light Attack`, `Defend`",
                    inline=False)
                user1_message = await user_1.send(embed=temp_embed,
                                                  components=[action_row])

                buttonctx: ComponentContext = await wait_for_component(
                    client, messages=user1_message.id, components=action_row)

                pressed, dealt, state = await fighter_state(
                    player1, player2, buttonctx)

                if player1.dmg == 0 and player1.state is not FightState.DEFEND:
                    msg = f"{player1.name}'s {state} missed\n!"
                    # remove the buttons
                    self.game_log += msg
                    embed_now = embed_new(
                        log=self.game_log,
                        self_hp=player1.hp,
                        opponent_hp=player2.hp
                    ).add_field(
                        name="Waiting for Opponent!",
                        value="Please wait while your opponent chooses an action!", inline=False)

                    await buttonctx.edit_origin(
                        # content=f"Your log: {msg}\nWaiting for opponent!",
                        embed=embed_now,
                        components=[])
                    # update user 2's msg no items here
                    await user2_message.edit(embed=embed_new(
                        log=self.game_log,
                        self_hp=player2.hp,
                        opponent_hp=player1.hp),
                        components=[])
                    # missed so the turn is lost and we forfeit control
                    self.turn = Turn.PLAYER_2
                    continue

                elif player1.state is FightState.DEFEND:
                    msg = f"{player1.name} chose to defend!\n"
                    self.game_log += msg
                    embed_now = embed_new(
                        log=self.game_log,
                        self_hp=player1.hp,
                        opponent_hp=player2.hp
                    ).add_field(
                        name="Waiting for Opponent",
                        value="Please wait while your opponent chooses an action!")
                    await buttonctx.edit_origin(embed=embed_now, components=[])
                    await user2_message.edit(
                        embed=embed_new(log=self.game_log,
                                        self_hp=player2.hp,
                                        opponent_hp=player1.hp))
                    self.turn = Turn.PLAYER_2
                    continue

                # if this attack is fatal
                if player2.hp <= 0:
                    # player 2 lost msg

                    await user2_message.edit(
                        # content=
                        # f"You lost! :( Your character got dealt {dealt} dmg with a {state} in the last hit!",
                        embed=quickembed().add_field(
                            name="Lost!", value="You lose! ",
                            inline=False).add_field(
                                name="Got attacked by",
                                value=state,
                                inline=True).add_field(
                                    name="DMG taken from last ATK",
                                    value=dealt,
                                    inline=True),
                        components=[])
                    # player 1 wins
                    await user1_message.edit(

                        # content=
                        # f"{player1.name} won by dealing {dealt} dmg with a {state} in the last hit!",
                        embed=quickembed().add_field(
                            name="Won!", value="You win!", inline=False).add_field(
                                name="Last ATK", value=state,
                                inline=True).add_field(
                                    name="DMG dealt by last ATK",
                                    value=dealt,
                                    inline=True),
                        components=[])
                    self.winner = player1.name
                    break

                # reach here, this means that attack is neither fatal, nor a miss,
                # can successfully attack and complete the turn now
                # player 2 msg
                logmsg = f"{player1.name} dealt {dealt} to {player2.name} with a {state}!\t{player2.name}'s current HP -> {player2.hp}\n"
                self.game_log += logmsg
                embed_now = add_dmg_recv(embed_new(log=self.game_log,
                                                   self_hp=player2.hp,
                                                   opponent_hp=player1.hp),
                                         got_hit_by=state,
                                         got_hit_for=dealt)
                await user2_message.edit(
                    # content=
                    # f"Got hit by a {state}! Got hit for {player1.dmg} HP! Current HP -> {player2.hp}",
                    embed=embed_now,
                    components=[])

                # player 1 msg
                embed_now = add_dmg_prod(embed_new(log=self.game_log,
                                                   self_hp=player1.hp,
                                                   opponent_hp=player2.hp),
                                         dealt=dealt,
                                         atktype=state)
                await user1_message.edit(
                    # content=
                    # f"{pressed}! Dealt {dealt} dmg to {player2.name}! Opponent's remaining hp -> {player2.hp}",
                    embed=embed_now,
                    components=[])
                # change turn
                self.turn = Turn.PLAYER_2
                continue

            # this is the second turn, so we switch them up.
            if self.turn is Turn.PLAYER_2:
                # this is second turn the responder attacks,
                # next time the args are flipped in `fighter_state()`
                temp_embed = quickembed().add_field(
                    name="Choose an Attack",
                    value="Pick any of the three actions: `Heavy Attack`, `Light Attack`, `Defend`"
                )
                user2_message = await user_2.send(embed=temp_embed,
                                                  components=[action_row])

                buttonctx: ComponentContext = await wait_for_component(
                    client, messages=user2_message.id, components=action_row)

                pressed2, dealt2, state2 = await fighter_state(
                    player2, player1, buttonctx)

                if player2.dmg == 0 and player2.state is not FightState.DEFEND:
                    msg = f"{player1.name}'s {state2} missed!\n"
                    # remove the buttons
                    self.game_log += msg
                    embed_now = embed_new(
                        log=self.game_log,
                        self_hp=player2.hp,
                        opponent_hp=player1.hp
                    ).add_field(
                        name="Waiting for Opponent!",
                        value="Please wait while your opponent chooses an action")
                    await buttonctx.edit_origin(
                        # content=f"Your log: {msg}\nWaiting for opponent!",
                        embed=embed_now,
                        components=[])
                    # we need to remove the components
                    await user2_message.edit(
                        # content=f"Chose {state2}! Waiting for opponent..",
                        embed=embed_new(log=self.game_log,
                                        self_hp=player2.hp,
                                        opponent_hp=player1.hp).
                        add_field(
                            name="Waiting for Opponent",
                            value="Please wait while your opponent chooses an action!"
                        ),
                        components=[])

                    self.game_log += msg
                    # update user 1's msg no items here
                    await user1_message.edit(embed=embed_new(
                        log=self.game_log,
                        self_hp=player1.hp,
                        opponent_hp=player2.hp),
                        components=[])
                    # missed so the turn is lost and we forfeit control
                    self.turn = Turn.PLAYER_1
                    continue

                elif player2.state is FightState.DEFEND:
                    msg = f"{player2.name} chose to defend!\n"
                    self.game_log += msg
                    embed_now = embed_new(
                        log=self.game_log,
                        self_hp=player2.hp,
                        opponent_hp=player1.hp
                    ).add_field(
                        name="Waiting for Opponent",
                        value="Please wait while your opponent chooses an action!")
                    await buttonctx.edit_origin(
                        # content=f"Your log: {msg}\nWaiting for opponent!",
                        embed=embed_now,
                        components=[])
                    await user1_message.edit(
                        embed=embed_new(log=self.game_log,
                                        self_hp=player1.hp,
                                        opponent_hp=player2.hp))
                    self.turn = Turn.PLAYER_1
                    continue

                # if this attack is fatal to player 1
                # we message p1 that they lost
                # and p2 that they won
                if player1.hp <= 0:
                    # player 2 lost msg
                    await user1_message.edit(
                        # content=
                        # f"You lost! :( Your character got dealt {dealt2} dmg with a {state2} in the last hit!",
                        embed=quickembed().add_field(
                            name="Lost!", value="You lose!",
                            inline=False).add_field(
                                name="Got attacked by",
                                value=state2,
                                inline=True).add_field(
                                    name="DMG taken from last ATK",
                                    value=dealt2,
                                    inline=True),
                        components=[])
                    # player 2 wins
                    await user2_message.edit(
                        # content=
                        # f"{player2.name} won by dealing {dealt2} dmg with a {state2} in the last hit!",
                        embed=quickembed().add_field(
                            name="Won!", value="You win!",
                            inline=False).add_field(
                                name="Last ATK", value=state2,
                                inline=True).add_field(
                                    name="DMG dealt by last ATK",
                                    value=dealt2,
                                    inline=True),
                        components=[])
                    self.winner = player1.name
                    break

                # reach here, this means that attack is neither fatal, nor a miss,
                # can successfully attack and complete the turn now
                # player 2 msg
                logmsg = f"{player2.name} dealt {dealt2} dmg to {player1.name}! {player1.name}'s current HP -> {player1.hp}\n"
                self.game_log += logmsg
                embed_now = add_dmg_prod(embed_new(log=self.game_log,
                                                   self_hp=player2.hp,
                                                   opponent_hp=player1.hp),
                                         dealt=dealt2,
                                         atktype=state2)
                await user2_message.edit(
                    # content=
                    # f"{pressed2}! Dealt {dealt2} dmg to {player1.name}!\t{player1.name}'s remaining hp -> {player1.hp}"
                    embed=embed_now,
                    components=[])
                # player 1 msg
                embed_now = add_dmg_recv(embed_new(log=self.game_log,
                                                   self_hp=player1.hp,
                                                   opponent_hp=player2.hp),
                                         got_hit_by=state2,
                                         got_hit_for=dealt2)

                await user1_message.edit(
                    # content=
                    # f"Got hit by a {state2}! Got hit for {player1.dmg} HP! Current HP -> {player1.hp} ",
                    embed=embed_now,
                    components=[])
                # change turn
                self.turn = Turn.PLAYER_1
                continue

        print("Fight Success!")
        return


# run the bot
client.run(token)
